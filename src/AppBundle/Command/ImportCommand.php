<?php
/**
 * Created by PhpStorm.
 * User: marc
 * Date: 14/11/16
 * Time: 21:46
 */

namespace AppBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:import')

            // the short description shown while running "php bin/console list"
            ->setDescription('Import SaaS products')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("Update our inventory of SaaS products from several sources")

            // argument to know what provider must be choosen
            ->addArgument('provider', InputArgument::REQUIRED, 'The provider to import files')

            // argument to know where file is
            ->addArgument('filename', InputArgument::REQUIRED, 'Filename of file to import')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $provider   =   $input->getArgument('provider');
        $filename   =   $input->getArgument('filename');

        $importService = $this->getContainer()->get('getapp.service.import');

        $import = $importService->importProvider($provider, $filename);

        if (is_array($import)) {
            foreach ($import as $info) {
                $output->writeln('importing: '.$info);
            }
        } else {
            $output->writeln('importing: '.$import);
        }
    }
}