<?php

namespace AppBundle\Service;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

class ImportService
{
    /**
     * @var EntityManager em
     */
    private $em;

    /**
     * @var string
     */
    private $dirBase;

    /**
     * @var array
     */
    private $providers;

    /**
     * ImportService constructor.
     * @param EntityManager $em
     * @param $importBaseDir
     */
    public function __construct(EntityManager $em, $importBaseDir, $providers)
    {
        // EntityManager to persist data
        $this->em = $em;

        // get base directory where providers must be upload the files
        $this->dirBase = $importBaseDir;

        // in case to want add new providers is so easy as add new parameters in service config file (app/config/services.yml)
        $this->providers = $providers;
    }

    /**
     * Get base directori where files must be
     * @return string
     */
    public function getDirBase()
    {
        return $this->dirBase;
    }

    /**
     * Get name of providers configured
     * @return array
     */
    public function getProviders()
    {
        return array_keys($this->providers);
    }

    /**
     * Get type of files which providers works with it
     * @return array
     */
    public function getTypeFilesProviders()
    {
        return $this->providers;
    }

    /**
     * Import files by provider
     * @param $provider
     * @param $filePath
     * @return string
     */
    public function importProvider($provider, $filePath)
    {
        // check if provider is configured
        if (in_array($provider, $this->getProviders())) {
            // get type of file for this provider
            $fileExtension = $this->getTypeFilesProviders()[$provider];

            // search file to import
            $finder = new Finder();

            $finder->files()
                ->in($this->getDirBase());

            $fileToImport = '';
            foreach ($finder as $file) {
                if ($file->getRelativePathname() == $filePath) {
                    $fileToImport = $file;
                    break;
                }
            }

            // check extension for this provider and then, import by correct function for the extension
            switch ($fileExtension) {
                case 'yaml':
                    return $this->importYamlFiles($fileToImport);
                    break;
                case 'json':
                    return $this->importJsonFiles($fileToImport);
                    break;
            }
        } else {
            return "Provider not found";
        }
    }

    /**
     * Import provider files with yaml extension
     * @param $file
     * @return array|int|mixed
     */
    public function importYamlFiles($file)
    {
        try {
            // parse de Yaml file
            $import = Yaml::parse(file_get_contents($file));

            // at this point, it imports the content from file, in real use, data must be persisted
            if (is_array($import)) {
                $info = array();
                foreach($import as $data){
                    $importing = '';
                    ksort($data);
                    foreach($data as $key => $value) {
                        $importing .= ucfirst($key).': "'.$value.'"; ';
                    }
                    $info[] = $importing;
                }
            } else {
                return $import;
            }

            return $info;
        } catch (ParseException $e) {
            return printf("Unable to parse the YAML string: %s", $e->getMessage());
        }
    }

    /**
     * Import provider files with json extension
     * @param $file
     * @return int|mixed
     */
    public function importJsonFiles($file)
    {
        try {
            // parse Json file
            $json_file = file_get_contents($file->getRealPath());
            $import = json_decode($json_file, true);

            // at this point, it imports the content from file, in real use, data must be persisted
            $info = array();
            foreach($import['products'] as $data) {
                $importing =
                    'Name: "' . (isset($data['title']) ? $data['title'] : '-') . '"; ' .
                    'Categories: ' . (is_array($data['categories']) ? implode(', ', $data['categories']) : $data['categories']) . '; ' .
                    'Twitter: ' . (isset($data['twitter']) ? $data['twitter'] : '-');
                $info[] = $importing;
            }

            return $info;
        } catch (\Exception $e) {
            return printf("Unable to parse the JSON string: %s", $e->getMessage());
        }
    }
}