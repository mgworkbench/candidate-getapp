Candidate GetApp
======

This assignment has been creat with Symfony framework (3.1.6)

Considerations:

- No unit test has made because my knowledge about unit tests is scarce and I haven't had time to look it

Installation steps:

- You have two ways to install, first, by composer (execute composer install after clone repository) or unzip the zip file
- In case to choose first way to install, while composer execute install order, you must press return when base parameters are requested

How to run the code:

- This project has some dummy data (a yaml file and a json file under a directory called feed-products), this data are located in this directory: src/Resources/data
- To run the code, in a terminal, navigate to the directory where the project is and execute:
    - bin/console app:import [provider] [file_to_import] >> bin/console app:import capterra feed-products/capterra.yaml

Where to find the code:

- Code are in a repository in BitBucket (https://mgworkbench@bitbucket.org/mgworkbench/candidate-getapp.git) and in a zip file in the response email
- The code with the importation feature are located in several files. This feature has been thought to use by command console. The importation core has been made as a service, thereby it can be used in anyplace
- Structure:
    - src/AppBundle/Command/ImportCommand.php >> code to use import command by command console
    - app/config/services.yml >> where service has been defined
    - src/Resources/data/ >> where dummy data has been located
    - src/Service/ImportService.php >> the core of importation feature

Others:

- Maybe with more time I would like try to do some unit test
